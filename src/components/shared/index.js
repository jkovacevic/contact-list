import Vue from 'vue';

// Custom form elements (inputs)
import CImageUpload from './custom-form-elements/CImageUpload.vue';
import CInput from './custom-form-elements/CInput.vue';
import CInputComplex from './custom-form-elements/CInputComplex.vue';

// Modals
import ModalDelete from './modals/ModalDelete.vue';

Vue.component('CImageUpload', CImageUpload);
Vue.component('CInput', CInput);
Vue.component('CInputComplex', CInputComplex);
Vue.component('ModalDelete', ModalDelete);
