const key = 'tq-contact-list';

export default {
  getContacts() {
    const data = JSON.parse(localStorage.getItem(key)) || [];
    return data;
  },
  setContacts(contacts) {
    const data = JSON.stringify(contacts);
    localStorage.setItem(key, data);
  },
};
