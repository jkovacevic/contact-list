import ls from './ls';

export default {
  getContacts() {
    return ls.getContacts();
  },
  setContacts(contacts) {
    ls.setContacts(contacts);
  },
};
