export default {
  setContacts(state, contacts) {
    state.contacts = contacts;
  },
  createContact(state, contact) {
    // Contact id should be unique, automatically generated when implemented in DB table
    contact.id = Math.round(Math.random() * 10000);
    contact.favorite = false;
    state.contacts.push(contact);
  },
  updateContact(state, contact) {
    state.contacts.find((oldContact) => {
      if (oldContact.id === contact.id) {
        oldContact.name = contact.name;
        oldContact.email = contact.email;
        oldContact.picture = contact.picture;
        oldContact.numbers = contact.numbers;
        return true;
      }

      return false;
    });
  },
  updateSearchValue(state, value) {
    state.searchValue = value;
  },
  toggleFavoriteContact(state, contactId) {
    state.contacts.find((contact) => {
      if (contact.id === contactId) {
        contact.favorite = !contact.favorite;
        return true;
      }

      return false;
    });
  },
  deleteContact(state, contactId) {
    const index = state.contacts.findIndex((contact) => contact.id === contactId);
    if (index > -1) {
      state.contacts.splice(index, 1);
    }
  },
};
