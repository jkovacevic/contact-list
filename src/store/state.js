export default {
  contacts: [],
  searchValue: '',
  translations: {
    add_new: 'Add new',
    add_number: 'Add number',
    cancel: 'Cancel',
    contacts_all: 'All contacts',
    contacts_favorite: 'My Favorites',
    delete: 'Delete',
    email: 'Email',
    full_name: 'Full name',
    label: 'Label',
    modal_delete_info: 'Are you sure you want to delete this contact?',
    number: 'Number',
    numbers: 'Numbers',
    save: 'Save',
  },
};
