export default {
  translations: (state) => state.translations,
  contacts: (state) => state.contacts,
  contact: (state) => (id) => {
    const contactId = +id;
    return state.contacts.find((contact) => contact.id === contactId) || {};
  },
  searchValue: (state) => state.searchValue,
  filteredContacts: (state) => (isFavorite) => {
    let filtered = state.contacts;

    if (isFavorite) {
      filtered = filtered.filter((contact) => contact.favorite);
    }

    const searchVal = state.searchValue.trim('').toLowerCase();
    if (searchVal) {
      filtered = filtered.filter((contact) => {
        const name = contact.name.trim().toLowerCase();
        return name.includes(searchVal);
      });
    }

    return filtered;
  },
};
