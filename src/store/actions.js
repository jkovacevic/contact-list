import api from '@/api';

export default {
  getContacts({ commit }) {
    const contacts = api.getContacts();
    commit('setContacts', contacts);
  },
  createContact({ commit, getters }, contact) {
    commit('createContact', contact);
    api.setContacts(getters.contacts);
  },
  updateContact({ commit, getters }, contact) {
    commit('updateContact', contact);
    api.setContacts(getters.contacts);
  },
  updateSearchValue({ commit }, value) {
    commit('updateSearchValue', value);
  },
  toggleFavoriteContact({ commit, getters }, contactId) {
    commit('toggleFavoriteContact', contactId);
    api.setContacts(getters.contacts);
  },
  deleteContact({ commit, getters }, contactId) {
    commit('deleteContact', contactId);
    api.setContacts(getters.contacts);
  },
};
