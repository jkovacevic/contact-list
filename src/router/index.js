import Vue from 'vue';
import VueRouter from 'vue-router';
import Contacts from '../views/Contacts.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    redirect: {
      name: 'ContactsAll',
    },
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: Contacts,
    redirect: {
      name: 'ContactsAll',
    },
    children: [
      {
        path: 'all',
        name: 'ContactsAll',
        component: () => import(/* webpackChunkName "contacts-all" */ '@/components/contacts/ContactsList.vue'),
      },
      {
        path: 'favorites',
        name: 'ContactsFavorites',
        component: () => import(/* webpackChunkName "contacts-favorites" */ '@/components/contacts/ContactsList.vue'),
      },
    ],
  },
  {
    path: '/contact/:id',
    name: 'ContactDetails',
    component: () => import(/* webpackChunkName "contact-details" */ '@/components/contact-details/ContactDetails.vue'),
  },
  {
    path: '/contact/:id/edit',
    name: 'ContactEdit',
    component: () => import(/* webpackChunkName "contact-edit" */ '@/components/contact-create/ContactCreate.vue'),
  },
  {
    path: '/contacts/create',
    name: 'ContactCreate',
    component: () => import(/* webpackChunkName "contact-create" */ '@/components/contact-create/ContactCreate.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
