import Vue from 'vue';
import store from '@/store';

Vue.filter('translate', (value) => {
  const { translations } = store.getters;
  return translations[value] || value;
});
